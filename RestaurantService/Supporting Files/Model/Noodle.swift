//
//  Noodle.swift
//  PosSystemMachine
//
//  Created by   on 2020/7/27.
//  Copyright © 2020 PosSystem. All rights reserved.
//

import Foundation
class Noodle{
    let mainCourse = ["馄饨面","炸酱面","日式乌龙面","排骨面" ,"牛肉面","阳春面","福州意面" ]
    let cooked = ["干","汤"]
    let appetizer = ["皮蛋豆腐" , "烫青菜" , "牛蒡丝","不需要"]
    let drink = ["可口可乐","柳橙汁","七喜","无糖绿茶"]
    let sectionName = ["料理方式","小菜","饮料"]
    let price = ["33" , "28" , "38" , "45" , "50" , "30" , "30"]
}

class SingleNoodle{
    let mainCourse = ["馄饨面","炸酱面","日式乌龙面","排骨面" ,"牛肉面","阳春面","福州意面"]
    let mainCoursePrice = ["23","18","28","35","40","20","20"]
    let appetizer = ["皮蛋豆腐" , "烫青菜" , "牛蒡丝"]
    let appetizerPrice = ["10","10","10"]
    let drink = ["可口可乐","柳橙汁","七喜","无糖绿茶"]
    let drinkPrice = ["7","8","7","8"]
    //section名称
    let sectionTitle = ["主食","小菜","饮料","价格"]
}

