
//

import UIKit

class SteakLaCarteViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , NumberDelegate{
    var number = 0
    func numberSend(num: Int, indexPath: IndexPath) {
        self.number = num
        self.numbers[indexPath.section][indexPath.row] = self.number
        self.mTableView.reloadData()
    }
    
    @IBOutlet var mTableView: UITableView!
    let store = SingleSteak()
    //紀錄不同section 的 cell數量
    let cellNumber = [SingleSteak().salad.count,SingleSteak().coldAppetizer.count,SingleSteak().hotAppetizer.count,SingleSteak().soup.count,SingleSteak().drink.count,1]
    // 各不同section 的 cell
    let single = [SingleSteak().salad,SingleSteak().coldAppetizer,SingleSteak().hotAppetizer,SingleSteak().soup,SingleSteak().drink]
    var numbers = [[Int](repeating: 0, count:SingleSteak().salad.count),[Int](repeating: 0, count:SingleSteak().coldAppetizer.count),[Int](repeating: 0, count:SingleSteak().hotAppetizer.count),[Int](repeating: 0, count: SingleSteak().soup.count),[Int](repeating: 0, count: SingleSteak().drink.count)]
    let singlePrice = [SingleSteak().saladPrice,SingleSteak().coldAppetizerPrice,SingleSteak().hotAppetizerPrice,SingleSteak().soupPrice,SingleSteak().drinkPrice]
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableCell()
    }
    
    //MARK: - User Interface
    func userInterface(){
    
    }
    
    //MARK: - Register Cell
    func registerTableCell(){
        self.mTableView.register(UINib(nibName: "LaCarteTableViewCell", bundle: nil), forCellReuseIdentifier: "LaCarteTableViewCell")
        self.mTableView.register(UINib(nibName: "OrderTotalCell", bundle: nil), forCellReuseIdentifier: "OrderTotalCell")
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cellNumber.count == section {
            return cellNumber[section - 1]
        }else {
            return cellNumber[section]
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return store.sectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != self.single.count {
            let cell: LaCarteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LaCarteTableViewCell", for: indexPath) as! LaCarteTableViewCell
            cell.delegate = self
            cell.mFoodImg.image = UIImage(named: self.single[indexPath.section][indexPath.row])
            cell.mNameLabel.text = self.single[indexPath.section][indexPath.row]
            cell.mPriceLabel.text = "$ \(self.singlePrice[indexPath.section][indexPath.row])"
            cell.indexPath = indexPath
            cell.mNumberLabel.text = "\(self.numbers[indexPath.section][indexPath.row])"
            return cell
        }else {
            let cell: OrderTotalCell = tableView.dequeueReusableCell(withIdentifier: "OrderTotalCell", for: indexPath) as! OrderTotalCell
            cell.mPriceLabel.text = "$ \(addPrice(add: singlePrice, multiply: numbers))"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == store.sectionTitle.count - 1 {
            return 50
        }else {
            return 153
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if store.sectionTitle.count == section {
            return store.sectionTitle[section - 1]
        }else {
            return store.sectionTitle[section]
        }
    }
    
    //MARK: - Btn Action
    @IBAction func SendEvent(_ sender: Any) {
        if addPrice(add: singlePrice, multiply: numbers) == 0 {
            let Controller = UIAlertController(title: "请选择商品", message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "确认", style: .default, handler: nil)
            Controller.addAction(okAction)
            self.present(Controller, animated: true, completion: nil)
        }else{
            let Controller = UIAlertController(title: "订单已送出", message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "确认", style: .default, handler: {(action : UIAlertAction) -> Void in
                self.dismiss(animated: true, completion: nil)
                self.order(list: self.single, number: self.numbers, price: self.singlePrice)
                self.navigationController?.popToRootViewController(animated: true)
            })
            Controller.addAction(okAction)
            self.present(Controller, animated: true, completion: nil)
        }
    }

    
    
    //MARK: - Other Func
    func addPrice(add: [[String]] , multiply :[[Int]]) -> Int {
        var total = 0
        for (section,row) in multiply.enumerated() {
            for (innerSection , innerRow ) in row.enumerated(){
                if innerRow != 0 {
                    total += Int(add[section][innerSection])! * innerRow
                }
            }
        }
        return total
    }
    
    func order(list: [[String]] , number: [[Int]] , price: [[String]]){
        var total = 0
        for (i , n) in number.enumerated(){
            for (j , m) in n.enumerated(){
                if m != 0 {
                    total = m * Int(price[i][j])!
                    OrderModel.LaCarteOrderDetail.append(["\(list[i][j])" , "數量 : \(m) 份" , "\(total)"])
                }
            }
        }
    }

}
