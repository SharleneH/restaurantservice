//
//  LaCarteTableViewCell.swift
//  PosSystemMachine
//
//  Created by   on 2020/7/28.
//  Copyright © 2020 PosSystem. All rights reserved.
//

import UIKit

protocol NumberDelegate : class {
    func numberSend(num : Int , indexPath: IndexPath)
}

class LaCarteTableViewCell: UITableViewCell {

    @IBOutlet var mFoodImg: UIImageView!
    @IBOutlet var mPriceLabel: UILabel!
    @IBOutlet var mNameLabel: UILabel!
    @IBOutlet var mNumberLabel: UILabel!
    @IBOutlet var mAddBtn: UIButton!
    @IBOutlet var mMinusBtn: UIButton!
    var mNumber = 0
    var indexPath = IndexPath()
    weak var delegate: NumberDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func minusEvent(_ sender: Any) {
        if mNumberLabel.text == "0" {
            self.mNumber = 0
            self.delegate!.numberSend(num: mNumber, indexPath: indexPath)
        }else {
            self.mNumber -= 1
            self.delegate!.numberSend(num: mNumber, indexPath: indexPath)
        }
    }
    
    @IBAction func addEvent(_ sender: Any) {
        if mNumberLabel.text != "99" {
            mNumber += 1
            self.delegate!.numberSend(num: mNumber, indexPath: indexPath)
        }else{
            mNumber = 99
            self.delegate!.numberSend(num: mNumber, indexPath: indexPath)
        }
    }
}
