//

//

import UIKit
//import AdColony

class OrderViewController: UIViewController , UITableViewDataSource , UITableViewDelegate
//                           ,AdColonyAdViewDelegate
{
//    weak var banner:AdColonyAdView?
    @IBOutlet var mAdView: UIView!
    var mComboOrderList = OrderModel.ComboOrderDetail
    var  mLaCarteOrderList = OrderModel.LaCarteOrderDetail
    @IBOutlet var mTableView: UITableView!
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCell()
        self.userInterface()
        print("I'm in OrderViewController : \(OrderModel.ComboOrderDetail)")
    }
    
    //MARK: - Register Cell
    func registerTableViewCell(){
        self.mTableView.register(UINib(nibName: "OrderDetailCell", bundle: nil), forCellReuseIdentifier: "OrderDetailCell")
        self.mTableView.register(UINib(nibName: "OrderTotalCell", bundle: nil), forCellReuseIdentifier: "OrderTotalCell")
    }
    
    //MARK: - User Interface()
    func userInterface(){
        self.mTableView.bounces = false
        self.mTableView.separatorColor = .lightGray
    
    }
    
    //MARK: -TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.mComboOrderList.count
        }else if section == 1{
            return self.mLaCarteOrderList.count
        }else{
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell : OrderDetailCell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailCell", for: indexPath) as! OrderDetailCell
            cell.comboList(detail: mComboOrderList[indexPath.row])
            return cell
            
        }else if indexPath.section == 1{
            let cell : OrderDetailCell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailCell", for: indexPath) as! OrderDetailCell
            cell.laCarteList(detail: mLaCarteOrderList[indexPath.row])
            return cell

        }else {
            let cell : OrderTotalCell = tableView.dequeueReusableCell(withIdentifier: "OrderTotalCell", for: indexPath) as! OrderTotalCell
            cell.isSelected = false
            cell.mPriceLabel.text = "$ \(addPrice(combo: self.mComboOrderList, single: self.mLaCarteOrderList))"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 1{
            return 200
        }else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "套餐"
        }else if section == 1 {
            return "单点"
        }else {
            return "总计"
        }
    }
    //MARK: - AD
//    func requestBanner() {
//        let adSize = AdColonyAdSizeFromCGSize(mAdView.frame.size)
//        AdColony.requestAdView(inZone: "CTT", with: adSize, viewController: self, andDelegate: self)
//    }
//    
//    // handle new banner
//    func adColonyAdViewDidLoad(_ adView: AdColonyAdView) {
//        if let oldBanner = self.banner {
//            // remove previous banner if exists
//            oldBanner.destroy()
//        }
//
//        // you can set AdView size to be the same as placement size
//        // AdView will take care about banner centering
//        let placementSize = self.mAdView.frame.size
//        adView.frame = CGRect(x: 0, y: 0, width: placementSize.width, height: placementSize.height)
//        
//        // add banner to view
//        self.mAdView.addSubview(adView)
//        
//        // store banner reference to be able to clear it later
//        self.banner = adView
//    }
//    // handler banner loading failure
//    func adColonyAdViewDidFail(toLoad error: AdColonyAdRequestError) {
//        print("Banner request failed with error: \(error.localizedDescription) and suggestion: \(error.localizedRecoverySuggestion!)")
//    }
//    func adColonyAdViewWillOpen(_ adView: AdColonyAdView) {
//        print("AdView will open fullscreen view")
//    }
//    
//    func adColonyAdViewDidClose(_ adView: AdColonyAdView) {
//        print("AdView did close fullscreen views")
//    }
//    
//    func adColonyAdViewWillLeaveApplication(_ adView: AdColonyAdView) {
//        print("AdView will send used outside the app")
//    }
//    
//    func adColonyAdViewDidReceiveClick(_ adView: AdColonyAdView) {
//        print("AdView received a click")
//    }

    //MARK: - Other Func
    func addPrice(combo: [[String]] , single: [[String]]) -> Int {
        var result = 0
        for j in combo {
            for (x , s ) in j.enumerated(){
                if x == j.count - 1{
                    result += Int(s)!
                }
            }
        }
        for i in single{
            for(x , s) in i.enumerated(){
                if x == i.count - 1{
                    result += Int(s)!
                }
            }
        }
        return result
    }
}
