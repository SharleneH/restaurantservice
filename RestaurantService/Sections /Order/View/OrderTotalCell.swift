//
//  OrderTotalCell.swift
//  PosSystemMachine
//
//  Created by Luana Huang on 2020/7/24.
//  Copyright © 2020 PosSystem. All rights reserved.
//

import UIKit

class OrderTotalCell: UITableViewCell {

    @IBOutlet var mPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
