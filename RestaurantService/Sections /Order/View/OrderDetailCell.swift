//
//  OrderTableViewCell.swift
//  PosSystemMachine
//
//  Created by   on 2020/7/24.
//  Copyright © 2020 PosSystem. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {
    @IBOutlet var mNameLabel: UILabel!
    @IBOutlet var mPriceLabel: UILabel!
    @IBOutlet var mTextView: UITextView!
    @IBOutlet var mImage: UIImageView!
    var total = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func comboList(detail: [String]) {
        print("This is Combo detail : \(detail)")
        self.mImage.image = UIImage(named: detail[detail.count - 2])
        self.mPriceLabel.text = "$ \(detail[detail.count - 1])"
        self.mNameLabel.text = detail[detail.count - 2]
        for (i , s) in detail.enumerated() {
            if i < detail.count - 2 {
                self.mTextView.text += "\(s)\n\n"
                print(i , detail.count - 2 )
            }
        }
    }
    
    func laCarteList(detail: [String]){
        print("This is La Carte detail : \(detail)")
        self.mImage.image = UIImage(named: detail[0])
        self.mNameLabel.text = detail[0]
        self.mTextView.text = detail[1]
        self.mPriceLabel.text = "$ \(detail[2])"
    }
}
