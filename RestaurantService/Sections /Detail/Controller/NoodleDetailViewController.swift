
//

import UIKit


class NoodleDetailViewController: UIViewController , UITableViewDelegate , UITableViewDataSource, SectionViewDelegate {
    var ImgName: String = ""
    var Price : String = ""
    func sectionView(_ sectionView: SectionView, _ didPressTag: Int, _ isExpand: Bool) {
        self.mIsExpendDataList[didPressTag] = !isExpand
        self.mTableView.reloadSections(IndexSet(integer: didPressTag), with: .automatic)
    }
    @IBOutlet var mTableView: UITableView!
    @IBOutlet var mTableViewHeaderImg: UIImageView!
    @IBOutlet var mTableViewHeaderNameLabel: UILabel!
    @IBOutlet var mTableViewHeaderPriceLabel: UILabel!
    //Section名稱
    let mSectionName = Noodle().sectionName
    //套餐選單細項
    let mNoodle = [Noodle().cooked ,  Noodle().appetizer, Noodle().drink ]
    //展開/關閉 菜單
    var mIsExpendDataList = [Bool](repeating: false, count: Noodle().sectionName.count)
    //被選擇的Cell
    var mSelectedCell = [String](repeating: "", count:Noodle().sectionName.count )
    //送出的菜單包含細項
    var mOrderDetail = [String](repeating: "", count: Noodle().sectionName.count + 2)
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userInterface()
        self.registerTableViewCell()
    }
    
    //MARK: - RegisterCell
    func registerTableViewCell(){
        self.mTableView.register(UINib(nibName: "SectionView", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionView")
        self.mTableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageTableViewCell")
        self.mTableView.register(UINib(nibName: "DetailContentTableViewCell", bundle: nil ), forCellReuseIdentifier: "DetailContentTableViewCell")
    }
    
    //MARK: - UI
    func userInterface(){
        self.mTableViewHeaderImg.image = UIImage(named: ImgName)
        self.mTableViewHeaderNameLabel.text = ImgName
        self.mTableViewHeaderPriceLabel.text = "$ \(Price)"
        self.mTableView.bounces = false
    }
    
    //MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        self.mIsExpendDataList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mIsExpendDataList[section] {
            return mNoodle[section].count
        } else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DetailContentTableViewCell", for: indexPath) as! DetailContentTableViewCell
        cell.mOptionLabel.text = self.mNoodle[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView : SectionView = tableView.dequeueReusableHeaderFooterView(withIdentifier:"SectionView") as! SectionView
        sectionView.isExpand = self.mIsExpendDataList[section]
        sectionView.buttonTag = section
        sectionView.delegate = self
        sectionView.mTitleLabel.text = self.mSectionName[section]
        sectionView.mSectionBtn.setImage(UIImage(named: self.mIsExpendDataList[section] == true ? "btn_arrow_down" : "btn_arrow")
            , for: .normal)
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mOrderDetail[self.mSelectedCell.count] = self.ImgName
        self.mOrderDetail[self.mSelectedCell.count + 1] = self.Price

        if self.mSelectedCell[indexPath.section] == ""{
            self.mSelectedCell[indexPath.section] = "\(indexPath.row)"
            //紀錄餐點細節
            self.mOrderDetail[indexPath.section] = "\(self.mSectionName[indexPath.section]) : \(mNoodle[indexPath.section][indexPath.row])"
        }else {
            tableView.deselectRow(at: IndexPath(row: Int(self.mSelectedCell[indexPath.section])!, section: indexPath.section), animated: false)
            self.mSelectedCell[indexPath.section] = "\(indexPath.row)"
            //紀錄餐點細節
            self.mOrderDetail[indexPath.section] = "\(self.mSectionName[indexPath.section]) : \(mNoodle[indexPath.section][indexPath.row])"
        }
    }
    
    //MARK: - Btn Action
    @IBAction func SendEvent(_ sender: Any) {
        var listFlag = false
        for i in self.mOrderDetail{
            if i == ""{
                let Controller = UIAlertController(title: "还有未选选单", message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "确认", style: .default, handler: {(action : UIAlertAction) -> Void in
                    self.dismiss(animated: false, completion: nil)
                    listFlag = false

                }
                )
                Controller.addAction(okAction)
                self.present(Controller, animated: false, completion: nil)
            }else {
                listFlag = true
            }
        }
        if listFlag == true{
            let Controller = UIAlertController(title: "订单已送出", message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "确认", style: .default, handler: {(action : UIAlertAction) -> Void in
                self.dismiss(animated: true, completion: nil)
                OrderModel.ComboOrderDetail.append(self.mOrderDetail)
                self.navigationController?.popToRootViewController(animated: true)
            }
            )
            Controller.addAction(okAction)
            self.present(Controller, animated: false, completion: nil)
        }
        
    }
}
