//

//

import UIKit

protocol  SectionViewDelegate : class {
    func sectionView(_ sectionView: SectionView , _ didPressTag: Int , _ isExpand: Bool)
}

class SectionView: UITableViewHeaderFooterView {
    @IBOutlet var mTitleLabel: UILabel!
    @IBOutlet var mSectionBtn: UIButton!
    weak var delegate: SectionViewDelegate?
    var buttonTag: Int!
    // Cell 的展開縮合狀態
    var isExpand: Bool!
    //點擊展開/縮和按鈕
    @IBAction func pressExprendBtnEvent(_ sender: Any) {
        self.delegate?.sectionView(self, self.buttonTag, self.isExpand)
    }
}
