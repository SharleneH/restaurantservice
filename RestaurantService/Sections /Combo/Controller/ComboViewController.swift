
//

import UIKit


class ComboViewController: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    //更改店家
    let store = Steak()
    
    @IBOutlet var mCollectionView: UICollectionView!    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionViewCell()
        userInterface()
    }
    
    //MARK: - Register CollectionViewCell
    func registerCollectionViewCell(){
        self.mCollectionView.register(UINib(nibName: "ComboCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ComboCollectionViewCell")
    }
    
    //MARK:  - User Interface
    func userInterface(){
        self.mCollectionView.bounces = false
    }
    
    //MARK: - CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return store.mainCourse.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ComboCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComboCollectionViewCell", for: indexPath) as! ComboCollectionViewCell
        cell.mImages.image = UIImage(named: store.mainCourse[indexPath.row])
        cell.mNameLabel.text = store.mainCourse[indexPath.row]
        cell.mPriceLabel.text = "$ \(store.price[indexPath.row])"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: self.view.frame.width, height: CGFloat(300))
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //更改店家Controller
        let vc = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! SteakDetailViewController
        vc.ImgName = store.mainCourse[indexPath.row]
        vc.Price = store.price[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }


}
